import org.ansj.domain.Result;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.NlpAnalysis;

import org.nlpcn.commons.lang.tire.domain.Forest;
import org.nlpcn.commons.lang.tire.library.Library;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @description: some desc
 * @author: jiashen
 * @date: 2021/10/6 上午3:03
 */
public class Test {

    public static void test() throws Exception {

        Forest forest = null;
        URL url = Test.class.getClassLoader().getResource("userLibrary.dic");
        System.out.println(url.getPath());

        forest = Library.makeForest(url.toString());//filepath为自定义词典路径

    //只关注这些词性的词
        Set<String> expectedNature = new HashSet<String>() {
            {
//            add("n");add("v");add("vd");add("vn");add("vf");
//            add("vx");add("vi");add("vl");add("vg");
//            add("nt");add("nz");add("nw");add("nl");
//            add("ng");add("userDefine");add("wh");add("nr");add("nr1");add("nr2");
//            add("nrj");add("nrf");add("ns");add("nsf");

//            n 名词
//            nr 人名
//            nr1 汉语姓氏
//            nr2 汉语名字
//            nrj 日语人名
//            nrf 音译人名
//            ns 地名
//            nsf 音译地名
//            nt 机构团体名
//            nz 其它专名
//            nl 名词性惯用语
//            ng 名词性语素
//            nw 新词
            }
        };
//        String str = "快手福袋罗永浩抖音直播间欢迎使用ansj_seg,(ansj中文分词)在这里如果你遇到什么问题都可以联系我.我一定尽我所能.帮助大家.ansj_seg更快,更准,更自由!" ;

        String str = "\"#月亮\\uD83C\\uDF19 月亮很亮，亮也没用，没用也亮。我很想你，想你没用，没用也想……#美好的夜晚\"";
        Result result = NlpAnalysis.parse(str, forest); //分词结果的一个封装，主要是一个List<Term>的terms
        System.out.println(result.getTerms());

        List<Term> terms = result.getTerms(); //拿到terms
        System.out.println(terms.size());

        for (int i = 0; i < terms.size(); i++) {
            String word = terms.get(i).getName(); //拿到词
            String natureStr = terms.get(i).getNatureStr(); //拿到词性
            if (expectedNature.contains(natureStr)) {
                System.out.println(word + ":" + natureStr);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        test();

    }
}

//private static class Inner {
//    static Forest forest;
//    static {
//        try {
//            forest = Library.makeForest(Utils.class.getResourceAsStream("/library/userLibrary.dic"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.exit(1);
//        }
//    }

