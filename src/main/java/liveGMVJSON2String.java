import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.hive.ql.exec.UDF;

import java.util.ArrayList;

/**
 * @description: some desc
 * @author: jiashen
 * @date: 2021/10/17 下午3:42
 */
public class liveGMVJSON2String extends UDF {

    public  static String evaluate(String data) {

        JSONObject dataObj = JSON.parseObject(data);

        ArrayList<String> outString = new ArrayList<>();

        ArrayList<String> resultString = new ArrayList<>();

        for (String key : dataObj.keySet()) {
            JSONObject gmvRow = (JSONObject) dataObj.get(key);
            String gmvdou;

            String gmv = gmvRow.getString("gmv").replaceAll("¥", "");
            if (gmv.contains("万"))
                gmvdou = String.valueOf(Double.parseDouble(gmv.replaceAll("万", "")) * 10000);
            else
                gmvdou = String.valueOf(gmv);
            String product_id = gmvRow.getString("product_id");
            String promotion_id = gmvRow.getString("promotion_id");

            String order_num = gmvRow.getString("order_num");

            String user_num = gmvRow.getString("user_num");

            String title = gmvRow.getString("title");
            String cover = gmvRow.getString("cover");
            String price = gmvRow.getString("price");
            String missing = gmvRow.getString("missing");

            outString.add(product_id);
            outString.add(promotion_id);
            outString.add(gmvdou);
            outString.add(order_num);
            outString.add(user_num);
            outString.add(title);
            outString.add(cover);
            outString.add(price);
            outString.add(missing);

            resultString.add(String.join("|", outString));
            outString.clear();
        }
        return resultString.size() > 0 ? String.join("\\", resultString)
                : "";
    }


}
