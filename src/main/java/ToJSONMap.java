import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.hive.ql.exec.UDF;

import java.util.HashMap;

/**
 * @description: some desc
 * @author: jiashen
 * @date: 2021/11/2 下午6:57
 */
public class ToJSONMap  extends UDF {

    public  String evaluate(String input,String key1,String key2) {

        JSONArray objects = JSONObject.parseArray(input);
        HashMap<String, String> outMap = new HashMap<>();

        for (int i = 0; i < objects.size(); i++) {
            outMap.put(objects.getJSONObject(i).getString(key1),objects.getJSONObject(i).getString(key2));
        }

        String out = JSONObject.toJSON(outMap).toString();

        return out;
    }

//    public static void main(String[] args) {
//        String str =
//                "[{\"dt\":\"2021-10-20\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-23\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-27\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-22\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-19\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-26\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-29\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-28\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-30\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-21\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-24\",\"pv_day\":\"6\"},{\"dt\":\"2021-11-01\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-25\",\"pv_day\":\"6\"},{\"dt\":\"2021-10-31\",\"pv_day\":\"6\"}]";
//        evaluate(str) ;
//    }
}
