import org.apache.hadoop.hive.ql.exec.UDF;

import java.util.ArrayList;

/**
 * @description: some desc
 * @author: jiashen
 * @date: 2021/10/19 下午10:53
 */
public class IsProvince extends UDF {

    public String evaluate(String provinceString) {
        if (provinceString.isEmpty() || "".equals(provinceString)) {
            return provinceString;
        }

        return isProvince(provinceString);

    }

    public static String isProvince(String provinceString) {
        ArrayList<String> province = new ArrayList<>();

        province.add("北京");
        province.add("天津");
        province.add("河北");
        province.add("山西");
        province.add("内蒙古");
        province.add("辽宁");
        province.add("吉林");
        province.add("黑龙江");
        province.add("上海");
        province.add("江苏");
        province.add("浙江");
        province.add("安徽");
        province.add("福建");
        province.add("江西");
        province.add("山东");
        province.add("河南");
        province.add("湖北");
        province.add("湖南");
        province.add("广东");
        province.add("广西");
        province.add("海南");
        province.add("重庆");
        province.add("四川");
        province.add("贵州");
        province.add("云南");
        province.add("西藏");
        province.add("陕西");
        province.add("甘肃");
        province.add("青海");
        province.add("宁夏");
        province.add("新疆");
        province.add("香港");
        province.add("澳门");
        province.add("台湾");

        String provinceFilter = provinceString.replaceAll("省|自治区|壮族自治区|回族自治区|维吾尔自治区", "");
        return province.contains(provinceFilter) ? provinceFilter : "";
    }
}
